<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 27.10.2017
 * Time: 9:17
 */


namespace werewolf8904\cmsi18n\forms;


use werewolf8904\cmsi18n\models\I18nMessage;
use werewolf8904\cmsi18n\models\I18nSourceMessage;
use werewolf8904\composite\base\CompositeForm;

/**
 * Class I18nForm
 *
 * @package backend\modules\werewolf8904\cmsi18n\models
 *
 * @property  I18nSourceMessage $modelI18nSource
 * @property  I18nMessage[]     $modelsI18nMessage
 */
class I18nForm extends CompositeForm
{
    public $models = [
        'modelI18nSource' => null,
        'modelsI18nMessage' => []
    ];
    public $modelsClass = [
        'modelI18nSource' => I18nSourceMessage::class,
        'modelsI18nMessage' => I18nMessage::class

    ];

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {

        return [
            'modelsI18nMessage' => \Yii::t('model_labels', 'Models I18n Message')
        ];
    }

}