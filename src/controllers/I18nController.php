<?php

namespace werewolf8904\cmsi18n\controllers;


use werewolf8904\cmscore\controllers\BackendController;
use werewolf8904\cmsi18n\forms\I18nForm;
use werewolf8904\cmsi18n\models\I18nMessage;
use werewolf8904\cmsi18n\models\I18nSourceMessage;
use werewolf8904\cmsi18n\models\search\I18nSearch;
use werewolf8904\cmsi18n\service\I18nService;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * I18nSourceMessageController implements the CRUD actions for I18nSourceMessage model.
 */
class I18nController extends BackendController
{
    public $searchClass = I18nSearch::class;
    public $class = I18nForm::class;
    public $compositeService = I18nService::class;

    public function actionExtract()
    {
        Yii::$app->session->setFlash('alert', [
            'body' => $this->module->webExtractMessage(),
            'options' => ['class' => 'alert-success']
        ]);

        return $this->redirect(['index']);
    }

    public function init()
    {
        parent::init();
        $this->index_view_param_filter = function ($params) {
            $params['languages'] = ArrayHelper::map(
                I18nMessage::find()->select('language')->distinct()->all(),
                'language',
                'language'
            );
            $params['categories'] = ArrayHelper::map(
                I18nSourceMessage::find()->select('category')->distinct()->all(),
                'category',
                'category'
            );

            return $params;
        };
    }


}
