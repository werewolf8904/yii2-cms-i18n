<?php

namespace werewolf8904\cmsi18n\service;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 05.12.2017
 * Time: 11:25
 */


use werewolf8904\cmsi18n\forms\I18nForm;
use werewolf8904\cmsi18n\models\I18nSourceMessage;
use werewolf8904\composite\service\BaseService;


/**
 * Class I18nService
 *
 */
class I18nService extends BaseService
{

    public $model_class = I18nForm::class;
    public $main_model = 'modelI18nSource';
    public $main_model_class = I18nSourceMessage::class;
    public $relations = [
        'modelsI18nMessage' => 'i18nMessages'

    ];
}