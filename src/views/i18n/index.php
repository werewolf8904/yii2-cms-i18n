<?php

use kartik\grid\GridView;
use yii\grid\ActionColumn;
use yii\grid\SerialColumn;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $searchModel \werewolf8904\cmsi18n\models\search\I18nSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/**
 * @var $categories array
 * @var $languages  array
 */

$this->title = Yii::t('backend', 'I18n Source Messages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="i18n-source-message-index">

    <div class="col-xs-2">
        <?php echo Yii::t('backend', 'Extract message'); ?>
        <?php echo Html::button(\yii\helpers\Html::a('<i class="fa fa-refresh"></i>', \yii\helpers\Url::toRoute('extract'), [
            'title' => Yii::t('backend', 'Extract'),
            'data-confirm' => Yii::t('backend', 'Are you sure you want to extract message?')
        ])); ?>
    </div>
    <p>
        <?php echo Html::a(Yii::t('backend', 'Create {modelClass}', [
            'modelClass' => 'I18n Source Message'
        ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php $columns = [
        ['class' => SerialColumn::class],
        [
            'attribute' => 'category',
            'filter' => $categories
        ],
        [
            'attribute' => 'language',
            'filter' => $languages,
            'format' => 'html',
            'value' => function ($model) {
                return implode('<br/>', \yii\helpers\ArrayHelper::map($model->i18nMessages, 'language', 'language'));
            }
        ],
        [
            'attribute' => 'translations',
            'format' => 'html',
            'value' => function ($model) {
                $out = [];
                foreach ($model->i18nMessages as $lang => $mes) {
                    $out[] = $lang . ':' . $mes->translation;
                }
                return implode('<br/>', $out);
            },
            'vAlign' => 'middle',
        ],
        'message:ntext',

        [
            'class' => ActionColumn::class,
            'template' => '{update} {delete}'
        ]
    ]; ?>
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $columns
    ]); ?>

</div>
