<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \werewolf8904\cmsi18n\forms\I18nForm */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="i18n-source-message-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->field($model->modelI18nSource, 'category')->textInput(['maxlength' => 32]) ?>

    <?php echo $form->field($model->modelI18nSource, 'message')->textarea(['rows' => 6]) ?>
    <?php
    $data = \yii\helpers\ArrayHelper::map(\werewolf8904\cmscore\models\Language::find()->all(), 'code', 'name');

    echo $form->errorSummary($model->modelsI18nMessage);
    echo $form->field($model, 'modelsI18nMessage')
        ->widget(\unclead\multipleinput\MultipleInput::class, [
            'min' => 0,
            'sortable' => true,
            'columns' => [

                [
                    'name' => 'id',
                    'options' =>
                        [
                            'type' => 'hidden'
                        ],
                    'enableError' => true
                ],
                [
                    'name' => 'language',
                    'title' => Yii::t('model_labels', 'language'),
                    'type' => \kartik\widgets\Select2::class,
                    'enableError' => true,
                    'options' => [
                        'data' => $data,
                        'options' => [
                            'placeholder' => '',
                            'prompt' => '-'
                        ],
                        'pluginOptions' => [
                            'allowClear' => false,

                        ]
                    ]
                ],
                [
                    'enableError' => true,
                    'title' => Yii::t('model_labels', 'translation'),
                    'name' => 'translation'

                ]
            ]
        ]);
    ?>
    <div class="form-group">
        <?php echo Html::submitButton($model->modelI18nSource->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->modelI18nSource->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
