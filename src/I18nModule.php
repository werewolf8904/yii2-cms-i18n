<?php

namespace werewolf8904\cmsi18n;

use werewolf8904\cmsi18n\models\I18nMessage;
use werewolf8904\cmsi18n\models\I18nSourceMessage;
use Yii;


/**
 * Class Module
 *
 * @package backend\modules\i18n
 */
class I18nModule extends \yii\base\Module
{


    public $extract_message_config_file;
    public $extract_message_command = 'message/extract';
    public $console_runner = \tebazil\runner\ConsoleCommandRunner::class;

    /**
     * @param \yii\i18n\MissingTranslationEvent $event
     */
    public static function missingTranslation($event)
    {
        $source = I18nSourceMessage::find()->where(['message' => $event->message, 'category' => $event->category])->one();
        if (!$source) {
            $source = new I18nSourceMessage(['message' => $event->message, 'category' => $event->category]);
            $source->save();
        }
        $message = I18nMessage::find()->where(['id' => $source->id, 'language' => $event->language])->one();
        $message = $message ?: new I18nMessage(['id' => $source->id, 'language' => $event->language]);

        $message->translation = $event->message;

        $message->save();
    }

    public function init()
    {
        parent::init();
        if (!Yii::$container->has(\tebazil\runner\ConsoleCommandRunner::class)) {
            Yii::$container->set(\tebazil\runner\ConsoleCommandRunner::class, function () {
                return new \tebazil\runner\ConsoleCommandRunner( \yii\helpers\ArrayHelper::merge(
                    require Yii::getAlias('@common/config/base.php'),
                    require Yii::getAlias('@common/config/console.php'),
                    require Yii::getAlias('@console/config/console.php')
                )
                );
            });
        }
    }

    /**
     * @return mixed
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function webExtractMessage()
    {
        $runner = Yii::$container->get($this->console_runner);

        $runner->run($this->extract_message_command, [$this->extract_message_config_file]);
        return $runner->getOutput();
    }

}
