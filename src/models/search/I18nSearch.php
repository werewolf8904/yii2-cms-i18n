<?php

namespace werewolf8904\cmsi18n\models\search;


use werewolf8904\cmsi18n\models\I18nSourceMessage;
use Yii;
use yii\data\ActiveDataProvider;


/**
 * CatalogProductSearch represents the model behind the search form about `I18n`.
 */
class I18nSearch extends I18nSourceMessage
{
    public $translations;
    public $language;


    /**
     * /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['language', 'category'], 'string'],
            [['translations', 'message'], 'safe']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['language'] = Yii::t('model_labels', 'Language');
        $labels['translations'] = Yii::t('model_labels', 'Translations');
        return $labels;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = static::find();

        $query->joinWith('i18nMessages');
        $query->groupBy(static::tableName() . '.id');
        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id
        ]);
        $tr = preg_split('/[\s,.]+/', $this->translations, null, PREG_SPLIT_NO_EMPTY);
        $mes = preg_split('/[\s,.]+/', $this->message, null, PREG_SPLIT_NO_EMPTY);
        $query->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'translation', $tr])
            ->andFilterWhere(['=', 'language', $this->language])
            ->andFilterWhere(['like', 'message', $mes]);

        return $dataProvider;
    }
}
