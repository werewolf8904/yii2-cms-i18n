<?php
/**
 * Created by PhpStorm.
 * User: werewolf
 * Date: 22.08.2018
 * Time: 22:08
 */

namespace werewolf8904\cmsi18n\bootstrap;


use yii\base\BootstrapInterface;
use Yii;

class Bootstrap  implements BootstrapInterface
{
    public function bootstrap($app)
    {
        if(!$app->hasModule('i18n'))
        {
            $app->setModule('i18n', [
                'class'=>\werewolf8904\cmsi18n\I18nModule::class,
                'extract_message_config_file'=>'@common/config\messages\db.php'
            ]);
        }

    }
}