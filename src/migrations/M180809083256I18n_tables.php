<?php

namespace werewolf8904\cmsi18n\migrations;

use werewolf8904\cmscore\db\Migration;

class M180809083256I18n_tables extends Migration
{

    public function safeUp()
    {
        $tableOptions = $this->tableOptions;

        $this->createTable('{{%i18n_source_message}}', [
            'id' => $this->primaryKey(),
            'category' => $this->string(32),
            'message' => $this->text()
        ], $tableOptions);

        $this->createTable('{{%i18n_message}}', [
            'id' => $this->integer()->notNull()->defaultValue(0),
            'language' => $this->string(16)->notNull(),
            'translation' => $this->text()
        ], $tableOptions);

        $this->addPrimaryKey('i18n_message_pk', '{{%i18n_message}}', ['id', 'language']);
        $this->addForeignKey('fk_i18n_message_source_message', '{{%i18n_message}}', 'id', '{{%i18n_source_message}}', 'id', 'cascade', 'restrict');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_i18n_message_source_message', '{{%i18n_message}}');
        $this->dropTable('{{%i18n_message}}');
        $this->dropTable('{{%i18n_source_message}}');
    }

}
